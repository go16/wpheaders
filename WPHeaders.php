<?php
/*
Plugin Name: WPHeaders
Plugin URI: https://go16.fr/projects/wpheaders/
Description: A Wordpress plugin to send basic security headers
Version: 1.0
Author: Jerome Saiz
Author URI: https://go16.fr
Copyright: 2018 OPFOR Intelligence
License: MIT license @ https://opensource.org/licenses/MIT
*/

defined( 'ABSPATH' ) or die( 'No direct access allowed' );

// configure headers to be sent
// TODO : add plugin user interface instead of hard-coding headers
// TODO : get CSP out of this plugin & configurable (existing CSP plugins not working properly)
$new_headers = array(
	'signature' => 'wpheaders', // used to identify our array later
	'X-Content-Type-Options' => 'nosniff',
	'X-XSS-Protection' => '1; mode=block',
	'Strict-Transport-Security' => 'max-age=31536000; includeSubDomains',
	'X-Frame-Options' => 'DENY',
	'Referrer-Policy' => 'strict-origin',
	'Content-Security-Policy' => 'default-src \'self\'; style-src \'self\' \'unsafe-inline\' https://ajax.googleapis.com https://fonts.googleapis.com https://fonts.gstatic.com ; img-src * ; font-src \'self\' https://fonts.googleapis.com https://fonts.gstatic.com; script-src \'self\' \'unsafe-inline\'; '
);

/**
 * Prepare & inject the new security headers
 *
 * @param void
 * @return
 * @throws
 */
if (! function_exists('go16_custom_headers')) {
	function go16_custom_headers($new_headers)
	{
		// make sure we are processing the correct array
		// (and not the WP object being passed around at send_headers calls)
		if (is_array($new_headers) && $new_headers['signature'] === 'wpheaders') {
			unset($new_headers['signature']);

			foreach ($new_headers as $header => $value) {
				header($header . ': ' . $value);
			}
		}

		return null;
	}
}

// hook action
// we rely on send_headers instead of wp_headers
// @see https://wordpress.stackexchange.com/questions/213988/wp-headers-vs-send-headers-when-to-use-each/214028#214028
add_action( 'send_headers', 'go16_custom_headers', 10, 1 );
do_action( 'send_headers', $new_headers );